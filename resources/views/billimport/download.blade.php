@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Download Bill Collection</div>
                    <div class="panel-body">
                        {{Form::open(['url'=>'downloadExcel/csv','method'=>'get'])}}
                                &nbsp;&nbsp;
                                <div class="col-md-12">
                                    {!! Form::label('Payment Month','',['class'=>'col-md-4 control-label'])!!}
                                    <div class="col-md-6">
                                    {!! Form::select('payMonth',$billAll,'',['id'=>'payMonth','class'=>'form-control']) !!}
                                        @if($errors->has('payMonth'))
                                            <span class="text-danger">{{$errors->first('payMonth')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <br> &nbsp;&nbsp;
                                <div class="col-md-12">
                                    {!! Form::label('Payment Year','',['class'=>'col-md-4 control-label'])!!}
                                    <div class="col-md-6">
                                    {!! Form::select('payYear',$yearAll,'',['id'=>'payYear','class'=>'form-control']) !!}
                                        @if($errors->has('payYear'))
                                            <span class="text-danger">{{$errors->first('payYear')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <br>
                                &nbsp;&nbsp;
                                <div class="col-md-12 text-right">
                                    <div class="col-md-10 form-group">
                                        <button class="btn btn-success btn-m">Download CSV</button>
                                    </div>         
                                </div>
                        </div>
                            
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
