@extends('layouts.app')

@section('content')
<div class="container" >
    @if ($message = Session::get('success'))
        <div align="center" class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">Import Bills from P&E Server</div>
                <div class="panel-body">
                       {!! Form::open(["url" =>"/billimport/importserver", "method" => "post", "class" => "form-vertical", 'files' => true]) !!}
                                <div class="form-group {{ $errors->has('bill_date')?'has-error':'' }} {{ isset($bill_date)?'has-error':'' }}">
                                    <label class="form-label" for="category">Select Date</label>
                                    {!! Form::text('bill_date', date('d-M-Y'), array('data-field' => 'date', 'class' => 'form-control')) !!}  
                                    @if(isset($bill_date))
                                        <span class='help-block'>{{ $bill_date }}</span>
                                    @endif                              
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success">Import Bills</button>
                                </div>
                        {!! Form::close() !!}
                   
                </div>
            </div>
        </div>
    </div>
</div>
<div id="dtbox"></div>


@endsection
  @section("extrajs")
        <script type="text/javascript">
            $(document).ready(function(){
                $("#dtbox").DateTimePicker();
            });
        </script>
    @endsection
