@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Import customer bill details from external source</div>
                <div class="panel-body">
                    <div class='col-md-12'>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                            </div>
                        @endif

                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ Session::get('error') }}
                            </div>
                        @endif


                       {!! Form::open(["url" =>"importExcel", "method" => "post", "class" => "form-filter form-material", 'files' => true]) !!}

                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::label('Document','',['class'=>'col-md-4 control-label']) !!}
                                    <div class="col-md-8">
                                        {!! Form::file('import_file',Request::old('import_file'),array('class'=>'input-sm form-control')) !!}
                                        
                                        {{ csrf_field() }}
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-4">
                                <div class="form-group text-right">
                                    <button class="btn btn-success btn-md">Import File</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
