@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
         @if ($message = Session::get('success'))
                            <div class="alert alert-success" role="alert">
                            <center> {{ Session::get('success') }}</center>
                            </div>
                        @endif

                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ Session::get('error') }}
                            </div>
                        @endif
            
        </div>
        

    </div>
	<div class="row">
		<div class="col-md-12">
			  <div class="row">
                <div class="col-md-12">
                    {{Form::open(['url'=>'correction','method'=>'get'])}}  
                         <div class="col-md-3 form-group">
                            <?php 
                                if(isset($_GET['C_CODE'])){
                                    $consumer_no = $_GET['C_CODE'];
                                }
                                else{
                                    $consumer_no = NULL;
                                }
                            ?>
                            {!! Form::text('C_CODE', $consumer_no, array('class'=>'form-control', 'placeholder'=>'Customer Number'))!!}
                        </div> 
                        <div class="col-md-3 form-group">
                            <?php 
                                if(isset($_GET['year'])){
                                    $year = $_GET['year'];
                                }
                                else{
                                    $year = NULL;
                                }
                            ?>
                            {!! Form::select('year',$yearAll, $year,array('class'=>'form-control'))!!}
                        </div>
                        <div class="col-md-3 form-group">
                            <?php 
                                if(isset($_GET['month'])){
                                    $month = $_GET['month'];
                                }
                                else{
                                    $month = NULL;
                                }
                            ?>
                            {!! Form::select('month', $monthAll, $month,array('class'=>'form-control'))!!}
                        </div> 
                          <div class="col-md-3 form-group">
                            {!! Form::submit('Search', array('class'=>'btn btn-default btn-md search-btn'))!!}
                        </div>
                        {!!Form::close()!!}
                </div>
            </div>
                <div class="col-md-12">
					<div class="table-responsive">
					<strong>List Bill Collections</strong>
						<table class="table table-hover table-condensed" >
						<thead>
						  <tr>
						    <th height="38" align="left">#</th>
						    <th height="38" align="left" class="col-md-2">Customer Code</th>
						    <th height="38" align="left" class="col-md-2">Customer Name</th>
                            <th height="38" align="left" class="col-md-2">Bill Number</th>
						    <th height="38" align="left" class="col-md-2">Bill Date</th>
						    
						    <th height="38" align="left" class="col-md-1"> Bill Amount</th>
                            <th >Action</th>
						  </tr>
						  </thead>
						  <tbody>
						  	@foreach($history_list as $key => $bill)
							<tr bgcolor="">
						    <td height="25" align="left">{{ $key + $history_list->firstItem()}}</td>
                            <td height="25" align="left">{{ $bill['C_CODE'] }}</td>
						    <td height="25" align="left">{{ $bill['C_NAME'] }}</td>
                            <td height="25" align="left">{{ $bill['BILL_NO'] }}</td>
						    <td height="25" align="left">{{ $bill['BILL_DATE'] }}</td>
						   
                            
                                {{ Form::open(["url" =>route('billimport.update',$bill['ID']), "method" => "put", "class" => "form-vertical"])}}
                                    <td height="25" align="left"><input type="text" name="amount" value="{{ $bill['BILL_AMT']}}"></td>

                                   <td> <input type="submit" name="" value="Update" class="btn btn-xs btn-primary"></td>
                                {{Form::close()}}
                                                                   
                            
							@endforeach
						</tbody>
						</table>
						    {{ $history_list->appends(['C_CODE'=>$consumer_no,'year'=>$year,'month'=>$month])->links() }}
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
