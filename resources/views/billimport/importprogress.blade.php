<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>P&E Billing</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}">
    <link rel="stylesheet" media="print" type="text/css" href="{{ asset('css/print-hide.css') }}">
    
</head>
<body>
<div class="lightbox-loader">
    <p class="progress">
        <span class="meter"></span>
        <span class="notice">Importing Bills. Please Wait - <b></b></span>
    </p>

   <p class="center print-button-wrapper" style="top: 100px">
      <a class="print-button">VIEW/PRINT</a>
   </p>

</div>

<script type="text/javascript" src="{{ asset('jquery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('jquery/jQuery.ajaxQueue.min.js') }}"></script>
<script>
var count = {{ $count }};
var counter = 0;
var checkInterval;

jQuery(function($){
   checkInterval = setInterval('checkStatus()', 1000);
});

function checkStatus() {
  url = "{{ route('billimport.progressCheck', ['bill_date'=>$bill_date,'count'=> $count]) }}" 
  url+= "?" + (Math.round(Math.random() * 10000))
   jQuery.ajaxQueue({
      url: url,
      type: 'get'
   }).done(function (importcount) {
      var meterLength = (importcount.count / count) * 100;

      if(importcount.count == count) {
        document.location='/billimport'
      }

      $('.lightbox-loader .progress .notice b').text(Math.floor(meterLength) + '%');
      $('.lightbox-loader .progress .meter').stop().animate({width: meterLength + '%'}, 1000, 'swing');
   });
}
</script>
</body>
</html>
