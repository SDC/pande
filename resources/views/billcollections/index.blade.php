@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				  <div class="panel-body">
                    {{Form::open(['url'=>'billcollections','method'=>'get'])}}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php
                                        if(isset($_GET['C_CODE'])) {
                                            $C_CODE = $_GET['C_CODE'];                            
                                        } else { $C_CODE=null;}
                                    ?>
                                    {{Form::text('C_CODE',$C_CODE, ['class' => 'form-control input-sm col-md-12', 'id'=>'C_CODE', 'autofocus','placeholder'=>'Customer Code'])}}
                                </div>
                            </div>
                            
                            <div class="col-md-2">
                                <div class="form-group">
                                    <button class="btn btn-success pull-left">
                                        <i class="glyphicon glyphicon-search lg"></i>&nbsp;Search
                                    </button>
                                </div>
                            </div>
                        </div>
                    {{Form::close()}}
                </div>
                </div>
                <div class="col-md-12">
					<div class="table-responsive">
					<strong>List Bill Collections</strong>
						<table class="table table-hover table-condensed" >
						<thead>
						  <tr>
						    <th height="38" align="left">#</th>
						    <th height="38" align="left" class="col-md-2">Customer Code</th>
						    <th height="38" align="left" class="col-md-2">Bill Number</th>
						    <th height="38" align="left" class="col-md-2">Receipt Number</th>
						    <th height="38" align="left" class="col-md-2">Customer Name</th>
						    <th height="38" align="left" class="col-md-2">Bill Paid Date</th>
						    <th height="38" align="left" class="col-md-2">Paid Amount</th>
						  </tr>
						  </thead>
						  <tbody>
						  	@foreach($bills as $key => $bill)
							<tr bgcolor="">
						    <td height="25" align="left">{{ $index++}}</td>
						    <td height="25" align="left">{{ $bill->C_CODE }}</td>
						    <td height="25" align="left">{{ $bill->BILL_NO }}</td>
						    <td height="25" align="left">{{ $bill->RECEIPT_NO }}</td>
						    <td height="25" align="left">{{ $bill->RECEIPT_NO }}</td>
						    <td height="25" align="left">{{ $bill->PAID_DATE}}</td>
						    <td height="25" align="left">{{ $bill->PAID_AMT}}</td>
							@endforeach
						</tbody>
						</table>
						{{$bills->appends($filter)->links()}}
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
