@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class='col-md-12'>
                     <center>
                        <h3 class="text-danger">Sorry for the inconvenience</h3>
                        This Service is Temporarily Unavailable. Please Try After Sometime
                    </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


