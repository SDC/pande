@extends('layouts.app')
@section('content')
<div class="container">
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading"> LIST OF USERS</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Name</th>
								<th>Email</th>
								<th>Control</th>
							</tr>
						</thead>
						<tbody>
							@foreach($userAll as $user)
							<tr>
								<td>{{ $index++ }}</td>
								<td>{{ $user->name }}</td>
								<td>{{ $user->email }}</td>
								<td>
									 {!! Form::open(array('url'=>route('profiles.destroy', array($user->id)),'method'=>'delete')) !!}
					                	<a href="{{ route('profiles.edit',$user->id) }}" class="btn btn-success btn-xs"><i class="fa fa-edit" data-content="Add customers to your feed"></i>EDIT</a> 
						               	<button class="btn btn-danger btn-xs" type="submit" onclick="return confirm ('<?php echo ('Are you sure') ?>');"><i class="fa fa-trash"></i>DELETE</button>
						            {!!Form::close() !!}
								</td>
							</tr>

							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">Profile</div>
			<div class="panel-body">
				{!! Form::open() !!}
				<div class="form-group">
					{{Form::label('Username',null)}}
					{{Form::text('test',null,['class' => 'form-control','placeholder' => 'Enter Email'])}}
				</div>
				<div class="form-group">
					{{Form::label('Email',null)}}
					{{Form::email('test',null,['class' => 'form-control','placeholder' => 'Enter Email'])}}
				</div>
				<div class="form-group">
					{{Form::label('Password',null)}}
					{{Form::text('test',null,['class' => 'form-control','placeholder' => 'Enter Password'])}}
				</div>
				<div class="form-group">
					{{Form::label('Confirm Password',null)}}
					{{Form::text('test',null,['class' => 'form-control','placeholder' => 'Confirm Password'])}}
				</div>
				{!! Form::close() !!}
			</div>
			<div class="panel-footer">
				<button class="btn btn-success">Update</button>
			</div>	

		</div>
	</div>	
</div>
@stop