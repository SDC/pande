<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Power Bill Online Payment Portal</title>

    <!-- Styles -->
    <style type="text/css">
    .modal {
    display:block;
    }
    </style>
    <style type="text/css">
.img-center {
    margin: 0 auto;
}

</style>
    <link href="/css/app.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link href="/css/DateTimePicker.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand"  href="{{ url('/') }}" style="margin-top:15px">
                   Mizoram Power Bill Online Payment
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                        &nbsp;
                </ul>
                @if(auth::guest())
                <!-- <ul class="nav navbar-nav navbar-right" id="guest-menu">
                    <li><a href="{{URL::to('/history_list')}}">History</a></li>
                    <li><a href="{{URL::to('/terms')}}">Terms and Conditions</a></li>
                    <li><a href="{{URL::to('/privacy')}}">Privacy Policy</a></li>
                    <li><a href="{{URL::to('/refund')}}">Refund Policy</a></li>
                    <li><a href="{{URL::to('/contact')}}">Contact Us</a></li>
                </ul>     -->       
                @else
                <ul class="nav navbar-nav navbar-left" style="margin-top:15px">
                    <li><a href="{{URL::to('/correction')}}">Bill Correction</a></li>
                    <li><a href="{{URL::to('/importExport')}}">Import Bill</a></li>
                    <li><a href="{{URL::to('/download')}}">Download Bill</a></li>
                    <li><a href="{{URL::to('/billimport')}}">Import Server</a></li>
                    <li><a href="{{URL::to('#')}}">Sync Server</a></li>
                    <li><a href="{{URL::to('/lockindex')}}">Open/Close Payment</a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right" style="margin-top:15px">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a> 
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ url('/logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                            <li class="divider"></li>
                            <li><a href="{{ url('/profiles') }}">Profile</a></li>
                        </ul>
                    </li>
                </ul>
                @endif
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

        @yield('content')
    </div>
     <footer class="footer">
      <div class="container">
        <p class="text-muted" align="center">This Portal belongs to Power & Electricity Department, Government of Mizoram <br>Developed by Mizoram State e-Governance Society(MSeGS)</p>
      </div>
    </footer
    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <script type="text/javascript" src="/js/DateTimePicker.js"></script>

    @yield('extrajs')
</body>
</html>
<style>

</style>
