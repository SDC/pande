@extends('layouts.app')

@section('content')
<div class="container">
        
       <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Bill Number : {{ $result['BILL_NO']}}</h4></div>
                <div class="panel-body">
                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Customer Name </strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result['C_NAME']}} </strong></h5>
                        </div>
                    </div> 

                    
                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Customer Code </strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result['C_CODE']}} </strong></h5>
                        </div>
                    </div>  
                  
                    <hr>   
                    
                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Bill Date</strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ date('d-M-y',strtotime($result['BILL_DATE']))}} </strong></h5>

                        </div>
                    </div> 
                  
                   <!--  <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Bill Period</strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{date('d-M-y',strtotime($result['BILL_FROM_DATE']))}} to {{ date('d-M-y',strtotime($result['BILL_TO_DATE']))}} </strong></h5>
                        </div>
                    </div> --> 
                  
                  
                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Bill Due Date</strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ date('d-M-y',strtotime($result['BILL_DUE_DATE']))}} </strong></h5>
                        </div>
                    </div>    
                  
                    <!-- <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Billed Units</strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result['BILLED_UNITS']}} </strong></h5>
                        </div>
                    </div>     -->
                  
                    <div class='col-md-12'>
                        <div class='col-md-5 text-danger'>
                            <h5><strong> Bill Amount</strong></h5>
                        </div>
                        <div class='col-md-7 text-danger'>
                           <h5><strong> :  &nbsp; Rs. {{ $result['BILL_AMT']}} </strong></h5>

                           <br>
                        </div>
                    </div> 

                    {{ Form::open(['url'=>'billpayment/query','class'=>'form-horizontal','files'=>'true']) }}
                        <input type="hidden" name="c_code" value="{{$result['C_CODE']}}"> 
                    <div class="col-md-12">
                        <div class="form-group">
                        <?php
                        $bill_count= App\BillCollections::where("BILL_NO", '=', $result['BILL_NO'])
                                                            ->where("STATUS", '=', 'SUCCESS')
                                                            ->count();
                        ?>
                        {{ Form::hidden('mobile_no', $mobile_no) }}
                        {{ Form::hidden('email_id', $email_id) }}
                        @if($bill_count>0)
                        <div class='text-danger text-center'>
                            <h3>Already Paid</h3>
                        </div>
                        @elseif(date('Y-m-d',strtotime($result['BILL_DUE_DATE'])) < date('Y-m-d'))
                        <div class='text-danger text-center'>
                            <h3>Bill Due Date Exceeded</h3>
                        </div>
                        @else
                        <div class="alert alert-info fade in">
                            <strong>Convenience fee for Online Payment</strong>
                                <ul>
                                    <li>Netbanking - Rs. 5/- plus Service Tax Per transaction processed below Rs.500 and Rs 7/- plus Service Tax Per transaction above Rs. 500/-</li>
                                    <li>Credit Card - 1.00%  of Transaction Value plus Service Tax</li>
                                    <li>Debit Card - 0.75% (Below 2000)/ 1.00% (Above Rs.2000) of bill amount (Plus Service Tax)</li>
                                    <li>Mobile Wallet - Rs.5/- of Transaction Value plus Service Tax Per transaction</li>
                                    <strong>Service Tax and other tax/taxes if any, will be charged in addition</strong>
                                </ul>
                        </div>
                        <label>
                            {{ Form::checkbox('agree') }} I agree to the <a href="/terms"> Terms & Condtions</a>,<a href="/privacy"> Privacy Policy </a> and <a href="/refund"> Refund Policy</a>
                        </label>
                            <button id='submit' class="btn btn-primary btn-lg btn-block" disabled="true" >Pay</button>
                        @endif
                        </div>
                    </div>
                    {{ Form::close()}}
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        $(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
            if($(this).is(":checked")){
               $('#submit').prop('disabled', false);
            }
            else if($(this).is(":not(:checked)")){
                $('#submit').prop('disabled', true);
            }
        });
    });
        </script>
        
</div>
@endsection
