@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div id="side" class="col-md-3 sidebar collapse navbar-collapse" style="background-color:#fff;padding-top:15px;padding-bottom:15px">
                    <ul class="nav nav-sidebar">
                        <li class="active"><a href="/">Pay Bill <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">Print Receipt</a></li>
                        <li><a href="#">View Payment History</a></li>
                    </ul>
                    <hr>
                    <ul class="nav nav-sidebar">
                        <li><a href="/terms">Terms & Conditions</a></li>
                        <li><a href="/privacy">Privacy Policy</a></li>
                        <li><a href="/refund">Refund Policy</a></li>
                    </ul>
                    <hr>
                    <div class="row" >
                        <div class="col-md-4 col-xs-4"><a href="https://power.mizoram.gov.in"><img src="/P&E Logo.jpg" width="100" height="70" class="img-responsive"></a></div>                
                        <div class="col-md-4 col-xs-4"><a href="https://msegs.mizoram.gov.in"><img src="/logo.png" width="100" height="70" class="img-responsive"></a></div>
                        <div class="col-md-4 col-xs-4"><a href="#"><img src="/billdesk.png" width="100" height="70" class="img-responsive"></a></div>
                    </div> 
                </div>
        <div class="col-md-9 ">
            <div class="panel panel-default">
                <div class="panel-heading text-center"><h3>Terms And Conditions</h3></div>
                <div class="panel-body">
                    <div class='col-md-12'>
                      <b>Payment Gateway/ Net Banking Disclaimer</b><br><br> 
                        Bill payments by you to Power & Electricity Department may be made through an electronic and automated collection and remittance service (the “Payment Gateway” & “ Internet Banking”) hosted by Power & Electricity Department’s designated bank. The Payment Gateway/ Internet Banking service is provided to you in order to facilitate access to view and pay your bills online. Power & Electricity Department makes no representation of any kind, express or implied, as to the operation of the Payment Gateway. You expressly agree that your use of this online payment service is entirely at your own risk. <br><br>

                       <b>Transaction charge</b><br><br>
                       Transaction charge will be borne by consumer. Transaction and Aggregation Related fees are given below:<br> 
                       <table class="table table-default">
                        <thead>
                          <th>
                            <tr>
                              <td>#</td>
                              <td>Mode</td>
                              <td>Total Fees</td>
                            </tr>
                          </th>
                        </thead>
                        <tr>
                            <td>1</td>
                            <td>Net Banking Facility Fee</td>
                            <td>Rs. 5/- plus Service Tax Per transaction processed below Rs.500.00</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Net Banking Facility Fee</td>
                            <td>Rs. 7/- plus Service Tax Per transaction processed above Rs.500.00</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Credit Card Gateway Facility Fee (Visa, MasterCard)</td>
                            <td>1.00% of Transaction Value plus Service Tax</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Debit Card Gateway Facility Fee (Visa, MasterCard/Ru-pay) below Rs.2000.00</td>
                            <td>0.75% of Transaction Value plus Service Tax per transaction  processed below Rs.2000.00</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Debit Card Gateway Facility Fee (Visa, MasterCard/Ru-pay) above Rs.2000.00</td>
                            <td>1.00% of Transaction Value plus Service Tax Per transaction  processed above Rs.2000.00</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Electronic Transaction processing fee for Wallets and cash cards</td>
                            <td>Rs.5/- of Transaction Value plus Service Tax Per transaction</td>
                        </tr>

                       </table>
                       <br><br>
                        <b>Receipt generation</b><br><br> 

                        You will get receipt of exact bill amount paid by you. An e-mail will also be sent to your registered 
                        e-mail address mentioning the payment information. 

                    <br><br><b>Limitation of Liability</b><br><br>     

                    Power & Electricity Department has made this service available to you as a matter of convenience. Power & Electricity Department expressly disclaims any claim or liability arising out of the provision of this service. You agree and acknowledge that you shall be solely responsible for your conduct and that Power & Electricity Department reserves the right to terminate your rights to use the service immediately. 
                        <br><br>
                   In no way shall Power & Electricity Department or its affiliates be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to, damages for loss of profits, goodwill, data or other intangible losses arising out of or in connection with use of the Payment Gateway.
                    <br><br>
                     Power & Electricity Department assumes no liability whatsoever for any monetary or other damage suffered by you on account of (i) the delay, failure, interruption, or corruption of any data or other information transmitted in connection with use of the Payment Gateway/ Internet Banking; or (ii) any interruption or errors in the operation of the Payment Gateway/ Internet Banking.  
                    <br><br>
                    You shall indemnify and hold harmless Power & Electricity Department and their respective officers, affiliates, agents, and employees, from any claim or demand, or actions. 
You agree, understand and confirm that personal data including without limitation details relating to debit card/credit card/ Net Banking transmitted over the internet is susceptible to misuse, theft and/or fraud and that Power & Electricity Department has no control over such matters. Although all reasonable care has been taken towards guarding against unauthorized use of any information transmitted by you, Power & Electricity Department does not represent or guarantee that the use of the payment gateway/ Internet Banking will not result in theft and/or unauthorized use of data over the internet. Debit card / Credit card / Net banking details. 

                    <br><br>
                    The debit/credit card/net banking details provided by you for use of the Payment Gateway/ Internet Banking will be correct and accurate and you shall not use a Debit card/Credit card/Net banking which is not lawfully owned by you. You further agree and undertake to provide correct and valid Debit card/Credit card/Net banking details. In default of the above conditions the Power & Electricity Department shall be entitled to recover the amount of transaction from the consumer against whose electricity bill the Credit card/Debit card/Net banking has been used. Further, the Power & Electricity Department also reserves the right to initiate any legal action for recovery of cost / penalty or any other punitive measure, as it may deem fit. 
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
@media (min-width: 768px){
    .navbar-collapse.collapse {
        width:25%;
    }
}
</style>
@endsection
