@extends('layouts.app')

@section('content')
    <div class="container">
        @if(Session::has('success'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('success') !!}</em></div>
        @endif
        <div class="row">
            <div id="side" class="col-md-3 sidebar">
                <div class="panel panel-default">
                  <div class="panel-body">
                    <div class="row" >
                        <div class="col-md-4 col-xs-4"><a href="https://power.mizoram.gov.in"><img src="/P&E Logo.jpg" width="100" height="70" class="img-responsive"></a></div>                
                        <div class="col-md-4 col-xs-4"><a href="https://msegs.mizoram.gov.in"><img src="/logo.png" width="100" height="70" class="img-responsive"></a></div>
                        <div class="col-md-4 col-xs-4"><a href="https://www.bharatbillpay.com" style="padding:10px;"><img src="/bbpslogo.jpg" width="100" height="70" class="img-responsive"></a></div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading"><strong>Useful Links</strong></div>
                  <div class="panel-body">
                    <ul>
                      <li><a href="https://youtu.be/_zj1C_bPhVE">Tuitorial Video</a></li>
                      <li><a href="https://bit.ly/2DLLjvU">Bharat BillPay Agent Locator</a></li>
                      <li><a href="https://www.bharatbillpay.com/customer.php?token=find-nearest-outlet">Find Nearest Outlet</a></li>
                    </ul>
                    
                  </div>
                </div>
            </div>
            <div class="col-md-5">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                    <div class="item active">
                     <a href="https://www.bharatbillpay.com/Billpay.php"><img src="banner_1.jpg" alt="Banner 1"></a> 
                    </div>
                    <div class="item">
                      <a href="https://www.bharatbillpay.com/customer.php?token=find-nearest-outlet"><img src="banner_2.jpg" alt="Banner 2"></a>
                    </div>

                    <div class="item">
                      <a href="https://www.bharatbillpay.com/customer.php?token=find-nearest-outlet"><img src="banner_3.png" alt="Banner 3"></a>
                    </div>

                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>                 
            </div>    
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Important Information</div>
                    <div class="panel-body">
                        <div class='col-md-12 alert alert-info fade in'>
                            <p>Mipuiin Electric Bill man chawi lova online a an pek theih nan P&E Department chuan hma a la a. <a href="https://www.bharatbillpay.com">Bharat BillPay</a> hmang a Electric Bill pe turin he link ah hian i click dawn nia</p>
                            <a href="https://www.bharatbillpay.com/Billpay.php">Please Click Here</a>
                        </div>
                   
                    </div>

                </div>
                
            </div>
        </div>

    </div>
<style type="text/css">
@media (min-width: 768px){
    .navbar-collapse.collapse {
        width:25%; 
    }
}
</style>
@endsection