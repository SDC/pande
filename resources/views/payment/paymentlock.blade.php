@extends('layouts.app')

@section('content')
    <div class="container">
        @if(Session::has('success'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('success') !!}</em></div>
        @endif
        <div class="col-md-12" >
                <div class="panel panel-default">
                    <div class="panel-body" >
                        <div  class='col-md-12' >
                            <center>

                                 {{ Form::open(["url" =>"/paymentlock", "method" => "get", "class" => "form-vertical"]) }} 
                                 @if($status)
                                    <h1>Payment is Closed</h1>
                                    <button class="btn btn-success btn-md">Open Payment</button>
                                @else
                                    <h1>Payment is Opened</h1>
                                    <button class="btn btn-danger btn-md">Close Payment</button>
                                 @endif
                                 {!! Form::close() !!}  

                            </center>
                        </div>
                    </div>
                </div>
        </div>            
    </div>
@endsection

