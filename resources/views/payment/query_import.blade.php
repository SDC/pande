@extends('layouts.app')

@section('content')
<div class="container">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">Bill</div>
                <div class="panel-body">
                    <div class='col-md-10'>
                        {{ Form::open(["url" =>"billpayment/query", "method" => "post", "class" => "form-filter form-material"]) }}
                            <div class='col-md-8'>
                                <div class='form-group {{ $errors->has('customer_id')?'has-error':'' }}'>
                                    {{ Form::text('customer_id', null , ['class' => 'form-control', 'placeholder' =>'Enter customer id']) }}

                                    @if($errors->has('customer_id'))
                                        <span class='help-block'>{{ $errors->first('customer_id') }}</span>
                                    @endif
                                </div>
                            </div>
                             <div class="col-md-4">
                                <div class="form-group text-right">
                                    <button class="btn btn-primary btn-md">SUBMIT</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

       <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Bill Number {{ $result['BILL_NO']}}</h4></div>
                <div class="panel-body">
                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Customer Name </strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result['C_NAME']}} </strong></h5>
                        </div>
                    </div> 

                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Customer Father's Name </strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result['C_FATHER_NAME'] }} </strong></h5>
                        </div>
                    </div> 

                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Customer Code </strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result['C_CODE']}} </strong></h5>
                        </div>
                    </div>  
                  
                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Sub Division </strong></h5>
                    <hr>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result['SUB_DIVISION']}} </strong></h5>
                    <hr>
                        </div>
                    </div>   
                    
                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Bill Date</strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result['BILL_DATE']}} </strong></h5>

                        </div>
                    </div> 
                  
                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Bill Date From</strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result['BILL_FROM_DATE']}} </strong></h5>
                        </div>
                    </div> 
                  
                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Bill Date To</strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result['BILL_TO_DATE']}} </strong></h5>
                        </div>
                    </div> 
                  
                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Bill Due Date</strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result['BILL_DUE_DATE']}} </strong></h5>
                        </div>
                    </div>    
                  
                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Billed Units</strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result['BILLED_UNITS']}} </strong></h5>
                        </div>
                    </div>    
                  
                    <div class='col-md-12'>
                        <div class='col-md-5 text-danger'>
                            <h5><strong> Bill Amount</strong></h5>
                        </div>
                        <div class='col-md-7 text-danger'>
                           <h5><strong> :  &nbsp; {{ $result['BILL_AMT']}} </strong></h5>

                           <br>
                        </div>
                    </div> 
                  
                    {!! Form::open(["url" =>"store_import", "method" => "post", "class" => "form-filter form-materia", 'files' => true]) !!}
                    
                        <input type="hidden" name="c_code" value="{{$result['C_CODE']}}"> 
                    <div class="col-md-12">
                        <div class="form-group">
                        <?php
                        $bill= App\BillCollections::where("BILL_NO", '=', $result['BILL_NO'])->count();
                        ?>
                        @if($bill>0)
                        <div class='text-danger text-center'>
                            <h3>Already Paid</h3>
                        </div>
                        @else
                            <button class="btn btn-primary btn-lg btn-block">Pay</button>
                        @endif
                        </div>
                    </div>
                    {{ Form::close()}}
                    <div class="col-md-6">
                    </div>

                </div>
            </div>
        </div>

</div>


@endsection
