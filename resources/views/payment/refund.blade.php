@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div id="side" class="col-md-3 sidebar collapse navbar-collapse" style="background-color:#fff;padding-top:15px;padding-bottom:15px">
                    <ul class="nav nav-sidebar">
                        <li class="active"><a href="/">Pay Bill <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">Print Receipt</a></li>
                        <li><a href="#">View Payment History</a></li>
                    </ul>
                    <hr>
                    <ul class="nav nav-sidebar">
                        <li><a href="/terms">Terms & Conditions</a></li>
                        <li><a href="/privacy">Privacy Policy</a></li>
                        <li><a href="/refund">Refund Policy</a></li>
                    </ul>
                    <hr>
                    <div class="row" >
                        <div class="col-md-4 col-xs-4"><a href="https://power.mizoram.gov.in"><img src="/P&E Logo.jpg" width="100" height="70" class="img-responsive"></a></div>                
                        <div class="col-md-4 col-xs-4"><a href="https://msegs.mizoram.gov.in"><img src="/logo.png" width="100" height="70" class="img-responsive"></a></div>
                        <div class="col-md-4 col-xs-4"><a href="#"><img src="/billdesk.png" width="100" height="70" class="img-responsive"></a></div>
                    </div>
                </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading text-center"><h3>Refund/Cancellation Policy</h3></div>
                <div class="panel-body">
                    <div class='col-md-12'>
                        Consumer has to ensure that Transaction Reference Number has been generated and if the same has not been generated, she/he has to make the payment of bill afresh to avoid disconnection. 

                        <br><br>Refund of the money due to above unsuccessful transaction The consumer has to take up the matter with the respective issuer bank. Power & Electricity Department will not be responsible for such refund. 
<br><br>
                        There are three ways of transaction flow –
                        <ul>
                         <li>Amount is debited from consumer’s account and success message is shown in Power & Electricity Department portal. Connection does not break. 

                        </li><li>Connection breaks before amount is debited from user’s account. There should not be any issue as consumer’s bank/card account is not debited.

                        </li><li>Connection breaks after amount is debited from user’s account but payment status is not shown in Power & Electricity Department portal. Successful payments will be settled during reconciliation process between Power & Electricity Department and Designated Bank/Billdesk. The consumers can pay again to make sure that he/she gets transaction receipt. In case of double (or multiple payment) of same bill, the extra amount will be saved as credit. This will be adjusted in the next bills. 
                        </ul>

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
@media (min-width: 768px){
    .navbar-collapse.collapse {
        width:25%;
    }
}
</style>

@endsection


