@extends('layouts.app')
@section('content')
<form action="https://pgi.billdesk.com/pgidsk/PGIMerchantPayment" method="POST" id="formdata" class="form-horizontal">
    {{ Form::text('msg', $data, ['class'=>'form-control', 'style'=>'width:100%']) }}
    <button type="submit" name="pay">Pay Now</button>
</form>
@endsection