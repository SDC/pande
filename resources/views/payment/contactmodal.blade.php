@extends('layouts.app')

@section('content')
    
<div id='loadModal'></div>s

<div class="modal fade bs-example-modal-sm" id="mymodal" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Contact Details
                    </div>
                    <div class="panel-body">
                        {{ Form::open(["url" =>"billpayment/pay", "method" => "post", "class" => "form-vertical"]) }}
                                <div class='form-group  {{ isset($emailValidation)?'has-error':'' }}'>
                                    {{ Form::text('email_id', null , ['class' => 'form-control', 'placeholder' =>'Enter email id']) }}
                                    @if(isset($emailValidation))
                                        <span class='help-block'>{{$emailValidation }}</span>
                                    @endif                                    
                                </div>
                                <div class='form-group {{ isset($mobileValidation)?'has-error':'' }} '>
                                    {{ Form::text('mobile_no', null , ['class' => 'form-control', 'placeholder' =>'Enter Mobile Number']) }}
                                    @if(isset($mobileValidation))
                                        <span class='help-block'>{{$mobileValidation }}</span>
                                    @endif  
                                </div>
                                {{ Form::hidden('consumer_no', $consumer_no) }}
                                
                                <div class="form-group">
                                    <button class="btn btn-primary btn-md">Proceed</button>
                                </div>
                        {!! Form::close() !!}   
                        <div >These details will be used to update Payment Status</div>    
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>


 <script type="text/javascript">
    $('#loadModal').ready(function(){
        $('#mymodal').modal('show');
    });
 </script>
@endsection

