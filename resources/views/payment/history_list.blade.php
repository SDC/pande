@extends('layouts.app')

@section('content')

    <div class="container">
            <div class="row">
                <div class="col-md-12">
                    {{Form::open(['url'=>'history','method'=>'get'])}}  
                         <div class="col-md-3 form-group">
                            <?php 
                                if(isset($_GET['c_code'])){
                                    $c_code = $_GET['c_code'];
                                }
                                else{
                                    $c_code = NULL;
                                }
                            ?>
                            {!! Form::text('c_code', $c_code, array('class'=>'form-control', 'placeholder'=>'Filter by Customer Code'))!!}
                        </div> 
                        <div class="col-md-3 form-group">
                            <?php 
                                if(isset($_GET['year'])){
                                    $year = $_GET['year'];
                                }
                                else{
                                    $year = NULL;
                                }
                            ?>
                            {!! Form::select('year',$yearAll, $year,array('class'=>'form-control'))!!}
                        </div>
                        <div class="col-md-3 form-group">
                            <?php 
                                if(isset($_GET['month'])){
                                    $month = $_GET['month'];
                                }
                                else{
                                    $month = NULL;
                                }
                            ?>
                            {!! Form::select('month', $monthAll, $month,array('class'=>'form-control'))!!}
                        </div> 
                          <div class="col-md-3 form-group">
                            {!! Form::submit('Search', array('class'=>'btn btn-default btn-md search-btn'))!!}
                        </div>
                        {!!Form::close()!!}
                </div>
            </div>
            <div class="col-md-12">
    			<div class="table-responsive">
    			     <strong>List Bill History</strong>
    				<table class="table table-hover table-condensed" >
    				<thead>
    				  <tr>
    				    <th height="38" align="left">#</th>
    				    <th height="38" align="left" class="col-md-2">Customer Code</th>
    				    <th height="38" align="left" class="col-md-2">Bill Number</th>
    				    <th height="38" align="left" class="col-md-2">Receipt Number</th>
    				    <th height="38" align="left" class="col-md-2">Bank Reference Number</th>
    				    <th height="38" align="left" class="col-md-1">Bill Paid Date</th>
    				    <th height="38" align="left" class="col-md-1">Paid Amount</th>
    				    <th height="38" align="left" class="col-md-2"></th>
    				  </tr>
    				  </thead>
    				  <tbody>
    				  	@foreach($history_list as $key => $history)

    					<tr bgcolor="">
                        <td height="25" align="left"> {{ $key + $history_list->firstItem() }}</td>
    				    <td height="25" align="left">{{ $history->C_CODE }}</td>
    				    <td height="25" align="left">{{ $history->BILL_NO }}</td>
    				    <td height="25" align="left">{{ $history->RECEIPT_NO }}</td>
    				    <td height="25" align="left">{{ $history->RECEIPT_NO }}</td>
    				    <td height="25" align="left">{{ $history->PAID_DATE}}</td>
    				    <td height="25" align="left">{{ $history->PAID_AMT}}</td>
    				    <td height="25" align="left">download</td>
    					@endforeach
    				</tbody>
    				</table>
                      {{ $history_list->links() }}
    		    </div>
    	    </div>
    </div>


@endsection
