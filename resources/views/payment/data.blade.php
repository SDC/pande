@extends('layouts.app')

@section('content')
<form action="https://pgi.billdesk.com/pgidsk/PGIMerchantPayment" method="POST" id="formdata">
    {{ Form::hidden('msg', $data) }}
</form>    
<script type="text/javascript">
  document.getElementById('formdata').submit();
</script>
@endsection

