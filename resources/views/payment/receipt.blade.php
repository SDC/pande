@extends('layouts.front')

@section('content')
<div class="container">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading text-center"><strong>Receipt Number : {{$result->RECEIPT_NO}}</strong></div>
                <div class="panel-body">
                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Customer Code </strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result->C_CODE}} </strong></h5>
                        </div>
                    </div> 

                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Bill No </strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result->BILL_NO}} </strong></h5>
                        </div>
                    </div>   

                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Paid Date </strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result->PAID_DATE}} </strong></h5>
                        </div>
                    </div> 

                    <div class='col-md-12'>
                        <div class='col-md-5'>
                            <h5><strong> Bill Date </strong></h5>
                        </div>
                        <div class='col-md-7'>
                           <h5><strong> :  &nbsp; {{ $result->BILL_DATE}} </strong></h5>
                        </div>
                    </div>        

                    <div class='col-md-12 text-danger text-center'>
                            <h4><strong> Bill Paid Successfully</strong></h4>
                        </div>
                    </div> 

                </div>
            </div>
        </div>
        <div class="col-md-3"></div>

</div>


@endsection
