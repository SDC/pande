@extends('layouts.app')

@section('content')
    <div class="container">
        @if(Session::has('success'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('success') !!}</em></div>
        @endif
        <div class="row">
                <div id="side" class="col-md-3 sidebar collapse navbar-collapse" style="background-color:#fff;padding-top:15px;padding-bottom:15px">
                    <ul class="nav nav-sidebar">
                        <li class="active"><a href="/">Pay Bill <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">Print Receipt</a></li>
                        <li><a href="#">View Payment History</a></li>
                    </ul>
                    <hr>
                    <ul class="nav nav-sidebar">
                        <li><a href="/terms">Terms & Conditions</a></li>
                        <li><a href="/privacy">Privacy Policy</a></li>
                        <li><a href="/refund">Refund Policy</a></li>
                    </ul>
                    <hr>
                    <div class="row" >
                        <div class="col-md-4 col-xs-4"><a href="https://power.mizoram.gov.in"><img src="/P&E Logo.jpg" width="100" height="70" class="img-responsive"></a></div>                
                        <div class="col-md-4 col-xs-4"><a href="https://msegs.mizoram.gov.in"><img src="/logo.png" width="100" height="70" class="img-responsive"></a></div>
                        <div class="col-md-4 col-xs-4"><a href="#"><img src="/billdesk.png" width="100" height="70" class="img-responsive"></a></div>
                    </div>
                </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Pay Bill</div>
                    <div class="panel-body">
                        <div class='col-md-12'>
                            {{ Form::open(["url" =>"billpayment/gGwXi", "method" => "post", "class" => "form-vertical"]) }}
                                    <div class='form-group {{ $errors->has('consumer_no')?'has-error':'' }} {{ isset($invalidConsumer)?'has-error':'' }} {{ isset($paymentDisable)?'has-error':'' }}'>
                                        {{ Form::text('consumer_no', null , ['class' => 'form-control', 'placeholder' =>'Enter Consumer Number']) }}

                                        @if($errors->has('consumer_no'))
                                            <span class='help-block'>{{ $errors->first('consumer_no') }}</span>
                                        @endif
                                        @if(isset($invalidConsumer))
                                            <span class='help-block'>{{ $invalidConsumer }}</span>
                                        @endif
                                        @if(isset($invalidCaptcha))
                                            <span class='text-danger'>{{ $invalidCaptcha }}</span>
                                        @endif
                                        @if(isset($paymentDisable))
                                            <span class='text-danger'>{{ $paymentDisable }}</span>
                                        @endif
                                    </div>
                                    <div class=" form-group g-recaptcha " data-sitekey="6Ld1QxcUAAAAAGSYEQehyQpZIMw5UDtxvs3cDjJk"></div>
                                    {{ Form::hidden('form_status', 'false') }}
                                    
                                    @if(\App\PaymentControl::orderBy('id','desc')->pluck('closed')->first())
                                    <div class="form group alert alert-danger fade in">
                                        Bill Payment has been closed for this Month
                                    </div>
                                    
                                    @else
                                        
                                        <div class="form-group">
                                        <button class="btn btn-primary btn-md">SUBMIT</button>
                                    </div>
                                    @endif
                                </div>

                            {!! Form::close() !!}         
                        </div>
        
                    </div>
                </div>
                 <div class="col-md-5">
                <div class="panel panel-default">
                    <div class="panel-heading">Important Information</div>
                    <div class="panel-body">
                        <div class='col-md-12 alert alert-info fade in'>
                            <strong>Convenience fee for Online Payment</strong>
                                <ul>
                                    <li>Netbanking - Rs. 5 (below Rs 500)/Rs. 7 (above Rs. 500) per transaction will be charged as convenience fee</li>
                                    <li>Credit Card - 1.00 % of bill amount</li>
                                    <li>Debit Card - 1.00% (Above 2000)/ 0.75% (upto Rs.2000) of bill amount</li>
                                    <li>Mobile Wallet - Rs. 5 per transaction will be charged as convenience fee</li>
                                    <strong>Service Tax and other tax/taxes if any, will be charged in addition</strong>
                                </ul>
                        </div>
                        <strong>Additional Information</strong>
                         <ul>
                                <li>Enter a valid <b>Email ID & Mobile Number </b> to receive Payment Details</li>
                                <li>You must agree to the <a href="/terms"> Terms & Condtions</a>,<a href="/privacy"> Privacy Policy </a> and <a href="/refund"> Refund Policy</a> to pay your bills online</li>
                                
                        </ul>       
        
                    </div>

                </div>
                
            </div>
        </div>        
    </div>
<style type="text/css">
@media (min-width: 768px){
    .navbar-collapse.collapse {
        width:25%; 
    }
}
</style>
@endsection