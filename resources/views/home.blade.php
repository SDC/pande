@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><center>WELCOME ADMINISTRATOR</center></div>
                    <div class="panel-body">
                        <div class="col-md-3" >
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <center>TRANSACTIONS AMOUNT <br> ({{date('Y')}}) <br><h3>&#8377; {{$yearTotal}}</h3> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <center>TRANSACTIONS AMOUNT <br>({{date('M')}}) <br><h3>&#8377; {{$monthTotal}}</h3> 
                                </div>
                            </div>
                        </div>
                         <div class="col-md-3">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <center>NO OF TRANSACTIONS <br>({{date('Y')}}) <br><h3> {{$yearCount}}</h3> 
                                </div>
                            </div>
                        </div>  
                        <div class="col-md-3">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <center>NO OF TRANSACTIONS <br>({{date('M')}}) <br><h3>{{$monthCount}}</h3> 
                                </div>
                            </div>
                        </div>  
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
