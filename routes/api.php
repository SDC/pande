<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
Route::get('bills', 'ApiController@bills');
Route::get('receipt', 'ApiController@receipt');
Route::get('get-outstanding','ApiController@getOutstanding');
Route::get('payment-post','ApiController@paymentPost');

