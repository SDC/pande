<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/



Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/', 'PaymentController@index');
Route::post('/billpayment/receipt','PaymentController@receipt');
Route::get('/billimport/progress-check/{bill_date}/{count}', array('uses' => 'BillImportController@progressCheck', 'as' => 'billimport.progressCheck'));

Route::get('/terms', 'PaymentController@terms');
Route::get('/privacy', 'PaymentController@privacy');
Route::get('/refund', 'PaymentController@refund');
Route::get('/contact', 'PaymentController@contact');
Route::post('/billpayment/pay', 'PaymentController@query');
Route::post('/billpayment/query', 'PaymentController@store');
Route::get('/querypage', 'PaymentController@queryPage');
Route::post('/billpayment/testURL', 'PaymentController@testUrl');
// Route::get('history_list', 'PaymentController@history_list');
// Route::get('history', 'PaymentController@history_list');

Route::get('/paymentlock','LockController@lock');
Route::get('/lockindex', 'LockController@index');

Route::post('/billpayment/gGwXi', 'PaymentController@getbill');
Route::get('/billpayment/{div}', 'PaymentController@billpayment');

Route::resource('/billpayment', 'PaymentController');

Route::post('/billpayment_import/query', 'PaymentController@query');
Route::post('store_import', 'PaymentController@store_import');
Route::get('/billpayment_import/{div}', 'PaymentController@billpayment_import');

Route::resource('/billpayment_import', 'PaymentController');

Route::post('/billimport','BillImportController@index');
Route::post('/billimport/importserver','BillImportController@import_server');
Route::resource('billcollections','BillCollectionsController');
Route::get('importExport','BillImportController@importExport');
Route::get('/download', 'BillImportController@download');
Route::post('importExcel', 'BillImportController@importExcel');
Route::get('downloadExcel/{type}', 'BillImportController@downloadExcel');
Route::resource('billimport','BillImportController');
Route::get('correction','BillImportController@billcorrection');


Route::get('transaction/count', 'TransactionsController@index');
Route::get('transaction/history', 'TransactionsController@create');
Route::resource('/profiles','ProfileController');