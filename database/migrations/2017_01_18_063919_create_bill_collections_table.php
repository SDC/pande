<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_collections', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('C_CODE');
            $table->string('BILL_NO');
            $table->string('PAID_AMT');
            $table->string('RECEIPT_NO');
            $table->string('TXNREFERENCENO');
            $table->string('BANKREFERENCENO');
            $table->string('BANKID');
            $table->string('NKMERCHANTID');
            $table->date('TXNDATE');
            $table->string('TXNTYPE');
            $table->string('email');
            $table->integer('MOBILE_NO');
            $table->date('PAID_DATE');
            $table->date('BILL_DATE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_collections');
    }
}
