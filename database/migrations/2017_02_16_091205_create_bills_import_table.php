<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsImportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('bills_import', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('C_CODE');
            $table->string('C_FATHER_NAME');
            $table->string('BILL_NO');
            $table->string('SUB_DIVISION');
            $table->string('BILLED_UNITS');
            $table->string('BILL_AMT');
            $table->date('BILL_DATE');
            $table->date('BILL_FROM_DATE');
            $table->date('BILL_TO_DATE');
            $table->date('BILL_DUE_DATE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('bills_import');
    }
}
