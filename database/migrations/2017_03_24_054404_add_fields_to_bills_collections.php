<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToBillsCollections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bill_collections', function (Blueprint $table) {
            $table->string('TXNREFERENCENO');
            $table->string('BANKREFERENCENO');
            $table->string('BANKID');
            $table->string('NKMERCHANTID');
            $table->date('TXNDATE');
            $table->string('TXNTYPE');
            $table->string('email');
            $table->integer('MOBILE_NO');
            $table->integer('TXNAMT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bill_collections', function (Blueprint $table) {
            $table->string('TXNREFERENCENO');
            $table->string('BANKREFERENCENO');
            $table->string('BANKID');
            $table->string('NKMERCHANTID');
            $table->date('TXNDATE');
            $table->string('TXNTYPE');
            $table->string('email');
            $table->integer('MOBILE_NO');
            $table->integer('TXNAMT');
        });
    }
}
