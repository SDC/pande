<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInBillCollections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bill_collections', function (Blueprint $table) {
            $table->string('BILLED_UNITS')->nullable();
            $table->string('BILL_DUE_DATE')->nullable();
            $table->string('BILL_DUE_AMT')->nullable();
            $table->string('SUB_DIVISION')->nullable();
            $table->string('SUB_DIVISION_DESC')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bill_collections', function (Blueprint $table) {
            //
        });
    }
}
