<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillCollectionsBBPS extends Model
{
     public $table = "bill_collections_bbps";
}