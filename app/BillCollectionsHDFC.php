<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillCollectionsHDFC extends Model
{
    public $table = "bill_collections_hdfc";
}
