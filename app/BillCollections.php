<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillCollections extends Model
{
     public $table = "bill_collections";
}
