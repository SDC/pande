<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillItem extends Model
{
     public $table = "bills_import";

      protected $fillable = [
        'C_CODE', 'BILL_NO', 'BILL_AMT','C_FATHER_NAME' , 'BILL_DATE','C_NAME','SUB_DIVISION'
        ,'BILLED_UNITS','BILL_TO_DATE','BILL_FROM_DATE','BILL_DUE_DATE'
    ];
}
