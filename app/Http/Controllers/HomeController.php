<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BillImport;
use App\BillCollections;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $yearTotal=BillCollections::whereYear('created_at',date('Y'))
                    ->where('status','SUCCESS')
                    ->sum('paid_amt');

        $monthTotal=BillCollections::whereMonth('created_at',date('m'))
                    ->whereYear('created_at',date('Y'))
                    ->where('status','SUCCESS')
                    ->sum('paid_amt');

        $monthCount=BillCollections::whereMonth('created_at',date('m'))
                    ->whereYear('created_at',date('Y'))
                    ->where('status','SUCCESS')
                    ->count();
        $yearCount=BillCollections::whereYear('created_at',date('Y'))
                    ->where('status','SUCCESS')
                    ->count();            

        setlocale(LC_MONETARY, 'en_IN');
        $yearTotal = money_format('%!i', $yearTotal);
        $monthTotal = money_format('%!i', $monthTotal);
        return view('home',compact('yearTotal','yearCount','monthCount','monthTotal'));;
    }
}
