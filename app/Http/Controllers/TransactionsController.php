<?php

namespace App\Http\Controllers;
use App\BillCollections;

use Illuminate\Http\Request;

class TransactionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$count = BillCollections::select('c_code')->distinct()
    							->where('status','=','SUCCESS')
    							->count();
    	return view('transactions.successfulCount',compact('count'));
    }

  }
