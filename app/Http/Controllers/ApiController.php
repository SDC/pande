<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BillCollections;
use App\BillCollectionsHDFC;
use App\BillItem;
use App\BillCollectionsBBPS;
use DB;

class ApiController extends Controller
{
    public function bills(Request $request){
        $api_key = [
                    "PP5ArfQpUvm3uS3T9HPHJEzsuJAvoOjHq7ac5ru49o="
                   ];

        if(in_array($request->get('api_key'), $api_key)){

            $billCollections = BillCollections::where(function ($q) use ($request){
                $q->where('C_CODE','=',$request->get('c_code'));
                $q->where('BILL_DUE_DATE','>',Date('Y-m-d'));
            })->orderBy('id','desc')->first();
            if(isset($billCollections))
            {
                if($billCollections->STATUS == 'SUCCESS')
                {
                    return 'Bill Already Paid';
                }
            } 
            if(isset($billCollections))
            {
                return response()->json([
                    'OPERATION' => 'OK',
                    'C_CODE' => $billCollections->C_CODE,
                    'BILL_NO' => $billCollections->BILL_NO,
                    'BILL_DATE' => $billCollections->BILL_DATE,
                    'BILL_DUE_DATE' => $billCollections->BILL_DUE_DATE,
                    'RECEIPT_NO' => $billCollections->RECEIPT_NO,
                    'BILL_DUE_AMT' => $billCollections->BILL_DUE_AMT,
                    'SUB_DIVISION' => $billCollections->SUB_DIVISION,
                    'SUB_DIVISION_DESC' => $billCollections->SUB_DIVISION_DESC,
                    'STATUS' => $billCollections->STATUS,
                ]);
            } 

            else 
            {
                $billItem = BillItem::where( function ( $q ) use ( $request ) {
                        if ( $request->get( 'c_code' ) ) {
                            $q->where( 'bills_import.C_CODE', '=', $request->get( 'c_code' ) );
                            $q->where('bills_import.BILL_DUE_DATE','>',Date('Y-m-d'));
                        }
                } )->orderBy('bills_import.id','desc')->first();
                if(isset($billItem))
                {
                    $last_id = BillCollections::orderBy('id','desc')->pluck('id','id')->first();
                    $number = ++$last_id;
                    $consumer_no = $billItem['C_CODE'];
                    $bill = new BillCollections;
                    $rcpno=substr($consumer_no,0,3).date('dmy').$number;
                    $bill->C_CODE=$billItem['C_CODE'];
                    $bill->BILL_NO=$billItem['BILL_NO'];
                    $bill->PAID_AMT=$billItem['BILL_AMT'];
                    $bill->BILLED_UNITS=$billItem['BILLED_UNITS'];
                    $bill->BILL_DUE_DATE=$billItem['BILL_DUE_DATE'];
                    $bill->BILL_DUE_AMT=$billItem['BILL_AMT'];
                    $bill->SUB_DIVISION=$billItem['SUB_DIVISION'];
                    $bill->SUB_DIVISION_DESC=$billItem['SUB_DIVISION'];
                    $bill->RECEIPT_NO=$rcpno;
                    $bill->STATUS="PENDING";
                    $bill->PAID_DATE=date('Y-m-d');
                    $bill->BILL_DATE=date('Y-m-d',strtotime($billItem['BILL_DATE']));
                    $bill->DISTRICT=$billItem['DISTRICT'];
                    $bill->C_NAME = $billItem['C_NAME'];   
                    $bill->PORTAL = 'BBPS';   
                    if($bill->save())
                    {
                        return response()->json([
                            'OPERATION' => 'OK',
                            'C_CODE' => $bill->C_CODE,
                            'BILL_NO' => $bill->BILL_NO,
                            'BILL_DATE' => $bill->BILL_DATE,
                            'BILL_DUE_DATE' => $bill->BILL_DUE_DATE,
                            'RECEIPT_NO' => $bill->RECEIPT_NO,
                            'BILL_DUE_AMT' => $bill->BILL_DUE_AMT,
                            'SUB_DIVISION' => $bill->SUB_DIVISION,
                            'SUB_DIVISION_DESC' => $bill->SUB_DIVISION_DESC,
                            'STATUS' => 'PENDING',
                        ]);
                    }
                } 
                else 
                {
                    return response()->json(['NOT FOUND']);
                }
            }
            
        } 
        else 
        {
            return response()->json('API Key Miss Match');
        }
    }

    public function receipt(Request $request){
        $api_key = [
            "PP5ArfQpUvm3uS3T9HPHJEzsuJAvoOjHq7ac5ru49o="
           ];
        if(in_array($request->get('api_key'), $api_key))
        {
            $billColByReceipt = BillCollections::where('RECEIPT_NO',$request['receipt_no'])
                                    ->where('C_CODE',$request['c_code'])
                                    ->orderBy('id','desc')->first();
            if(isset($billColByReceipt))
            {
                if($billColByReceipt->STATUS == 'PENDING')
                {
                    if($billColByReceipt->BILL_DUE_AMT == $request['paid_amt'] )
                    {

                        $billCollections = BillCollections::find($billColByReceipt->id);
                        $billCollections->STATUS            =  $request['status'];
                        $billCollections->TXNREFERENCENO    =  $request['txnreferenceno'];
                        $billCollections->BANKREFERENCENO   =  $request['bankreferenceno'];
                        $billCollections->BANKID            =  $request['bankid'];
                        $billCollections->NKMERCHANTID      =  $request['nkmerchantid'];
                        $billCollections->TXNDATE           =  date('Y-m-d',strtotime($request['txndate']));
                        $billCollections->TXNTYPE           =  $request['txntype'];
                        $billCollections->PAID_DATE         =  date('Y-m-d');
                        $billCollections->email             =  $request['email'];
                        $billCollections->MOBILE_NO         =  $request['mobile_no'];
                        $billCollections->TXNAMT            =  $request['txnamt'];
                        $billCollections->PAID_AMT          =  $request['paid_amt'];
                        if($billCollections->save())
                        {
                            return "SUCCESS UPDATED";
                        } 
                        else 
                        {
                            return "WRITE FAILURE";
                        }
                    }
                    else
                    {
                        return "TRANSACTION AMOUNT MISMATCH";
                    }
                } 
                else 
                {
                    return "DUPLICATE TRANSACTION";
                }
            } 
            else 
            {
                return "TRANSACTION NOT EXIST";
            }
        } 
        else 
        {
            return "API Key Miss Match";
        }

    }

    public function getOutstanding(Request $request){
       
        $api_key =[
            "$2y$10$5iXBStAy6VHjTb9l2l5G5OWTEmDbCJBb/6ST8ad2acYBERZ1.0YTu"
        ];
        if(in_array($request->get('api_key'), $api_key))
        {
            $billCollections = BillCollectionsHDFC::where(function ($q) use ($request){
                $q->where('C_CODE','=',$request->get('c_code'));
                $q->where('BILL_DUE_DATE','>',Date('Y-m-d'));
            })->orderBy('id','desc')->first();
            if(isset($billCollections))
            {
                if($billCollections->STATUS == 'SUCCESS')
                {
                    return 'Bill Already Paid';
                }
            }    
            if(isset($billCollections))
            {
                return response()->json([
                    'Message' => 'OK',
                    'Consumer No' => $billCollections->C_CODE,
                    'Consumer Name' => $billCollections->C_NAME,
                    'Bill No' => $billCollections->BILL_NO,
                    'Invoice Date' => $billCollections->BILL_DATE,
                    'Due Date' => $billCollections->BILL_DUE_DATE,
                    'Invoice No' => $billCollections->RECEIPT_NO,
                    'Amount' => $billCollections->BILL_DUE_AMT,
                    'Location' => $billCollections->SUB_DIVISION,
                    'Status' => $billCollections->STATUS,
                ]);
            } 
            else 
            {
                $billItem = BillItem::where( function ( $q ) use ( $request ) {
                        if ( $request->get( 'c_code' ) ) {
                            $q->where( 'bills_import.C_CODE', '=', $request->get( 'c_code' ) );
                            $q->where('bills_import.BILL_DUE_DATE','>',Date('Y-m-d'));
                        }
                } )->orderBy('bills_import.id','desc')->first();
                if(isset($billItem))
                {
                    $last_id = BillCollectionsHDFC::orderBy('id','desc')->pluck('id','id')->first();
                    $number = ++$last_id;
                    $consumer_no = $billItem['C_CODE'];
                    $bill = new BillCollectionsHDFC;
                    $rcpno=substr($consumer_no,0,3).date('dmy').$number;
                    $bill->C_CODE=$billItem['C_CODE'];
                    $bill->BILL_NO=$billItem['BILL_NO'];
                    $bill->PAID_AMT=$billItem['BILL_AMT'];
                    $bill->BILLED_UNITS=$billItem['BILLED_UNITS'];
                    $bill->BILL_DUE_DATE=$billItem['BILL_DUE_DATE'];
                    $bill->BILL_DUE_AMT=$billItem['BILL_AMT'];
                    $bill->SUB_DIVISION=$billItem['SUB_DIVISION'];
                    $bill->SUB_DIVISION_DESC=$billItem['SUB_DIVISION'];
                    $bill->RECEIPT_NO=$rcpno;
                    $bill->STATUS="PENDING";
                    $bill->PAID_DATE=date('Y-m-d');
                    $bill->BILL_DATE=date('Y-m-d',strtotime($billItem['BILL_DATE']));
                    $bill->DISTRICT=$billItem['DISTRICT'];
                    $bill->C_NAME = $billItem['C_NAME'];   
                    if($bill->save())
                    {
                        return response()->json([
                            'Message' => 'OK',
                            'Consumer No' => $bill->C_CODE,
                            'Consumer Name' => $bill->C_NAME,
                            'Bill No' => $bill->BILL_NO,
                            'Invoice Date' => $bill->BILL_DATE,
                            'Due Date' => $bill->BILL_DUE_DATE,
                            'Invoice No' => $bill->RECEIPT_NO,
                            'Amount' => $bill->BILL_DUE_AMT,
                            'Location' => $bill->SUB_DIVISION,
                            'Status' => 'PENDING',
                        ]);
                    }
                } 
                else 
                {
                    return response()->json(['NOT FOUND']);
                }
            }
            
        } 
        else 
        {
            return response()->json('API Key Miss Match');
        }
    }
    public function paymentPost(Request $request){
        $api_key = [
            "$2y$10$5iXBStAy6VHjTb9l2l5G5OWTEmDbCJBb/6ST8ad2acYBERZ1.0YTu"
           ];
        if(in_array($request->get('api_key'), $api_key))
        {
            $billColByReceipt = BillCollectionsHDFC::where('RECEIPT_NO',$request['receipt_no'])
                                    ->where('C_CODE',$request['c_code'])
                                    ->orderBy('id','desc')->first();
            if(isset($billColByReceipt))
            {
                if($billColByReceipt->STATUS == 'PENDING')
                {
                    if($billColByReceipt->BILL_DUE_AMT == $request['payment_amount'] )
                    {

                        $billCollections = BillCollectionsHDFC::find($billColByReceipt->id);
                        $billCollections->STATUS            =  $request['status'];
                        $billCollections->PAYMENT_GATEWAY   =  $request['payment_gateway'];
                        $billCollections->PAYMENT_MODE      =  $request['payment_mode'];
                        $billCollections->TRANSACTION_ID    =  $request['transaction_id'];
                        $billCollections->PAYMENT_DATE      =  date('Y-m-d');
                        $billCollections->PAID_AMT          =  $request['payment_amount'];
                        $billCollections->CARD_NUMBER       =  $request['card_no'];
                        if($billCollections->save())
                        {
                            return "SUCCESS UPDATED";
                        } 
                        else 
                        {
                            return "WRITE FAILURE";
                        }
                    }
                    else
                    {
                        return "TRANSACTION AMOUNT NOT MISMATCH";
                    }
                } 
                else 
                {
                    return "DUPLICATE TRANSACTION";
                }
            } 
            else 
            {
                return "TRANSACTION NOT EXIST";
            }
        } 
        else 
        {
            return "API Key Miss Match";
        }

    }
}
