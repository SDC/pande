<?php

namespace App\Http\Controllers;

use App\Payment;
use App\BillItem;
use DB;
use App\BillCollections;
use App\PaymentControl;
use App\webserver_details;
use Illuminate\Http\Request;
use Auth;
class LockController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        if(Auth::user()->email == 'hlira@mail.com' || Auth::user()->email == 'admin@mail.com')
        {
            $status = PaymentControl::orderBy('id','desc')->pluck('closed')->first();
            return view('payment.paymentlock',compact('status')); 
        }
        else
        {
            abort(403);
        }
    }
    public function lock()
    {
       
        $status = PaymentControl::orderBy('id','desc')->pluck('closed')->first();
        if($status)
        {
            PaymentControl::where('closed','1')
                            ->update(['closed'=>'0']);
        }
        else
        {
            PaymentControl::where('closed','0')
                        ->update(['closed'=>'1']);
        }
        $status = PaymentControl::orderBy('id','desc')->pluck('closed')->first();
        return redirect('/lockindex');
        
    }

}
