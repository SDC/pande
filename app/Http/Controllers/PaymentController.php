<?php

namespace App\Http\Controllers;

use App\Payment;
use App\BillItem;
use DB;
use App\BillCollections;
use App\PaymentControl;
use App\webserver_details;
use Illuminate\Http\Request;
use Auth;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // $remote_db=config('oracle');   
        // $port="1521";
        // if(@fsockopen($remote_db['oracle']['host'], $port,$errno, $errstr, 10))
        if(Auth::user())
        {
            return redirect('/home');
        }
        else {
            return view('payment.index');        
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getReceipt()
    {
        $bill_collections = (new BillCollections)->getTable();
        $bill_item = (new BillItem)->getTable();
        $consumer = BillItem::join($bill_collections,$bill_collections.'.c_code','=',$bill_item.'.c_code')
                    ->where($bill_collections.'.c_code','=','ARN101E034s')
                    ->get();
    }
    public function lockIndex()
    {
        $status = PaymentControl::orderBy('id','desc')->pluck('closed')->first();
        return view('payment.paymentlock',compact('status'));
    }
    public function lockPayment()
    {
       
        $status = PaymentControl::orderBy('id','desc')->pluck('closed')->first();
        if($status)
        {
            PaymentControl::where('closed','1')
                            ->update(['closed'=>'0']);
        }
        else
        {
            PaymentControl::where('closed','0')
                        ->update(['closed'=>'1']);
        }
        $status = PaymentControl::orderBy('id','desc')->pluck('closed')->first();
        return redirect('/lockindex');
        
    }
    public function create()
    {
        
    }
    public function terms ()
    {
        return view('payment.term');
    } 
    public function history(Request $request)
    {
        return view('payment.history');
    }
    public function privacy ()
    {
        return view('payment.privacy');
    }
    public function refund ()
    {
        return view('payment.refund');

    }
    public function contact ()
    {
        return view('payment.contact');

    }
    public function migrateBill()
    {
        
    }
    public function store(Request $request)
    {
        
        // $rules = [
        //         'checkbox' => 'required'    
        //     ];
        //     $this->validate($request, $rules);

        // Gosa Code Start
        /* $d_date= BillCollections::orderBy('id','desc')->pluck('PAID_DATE','id')->first();
        $c_date=date('Y-m-d');
        if($d_date == $c_date)
        {
            $p_date= BillCollections::orderBy('id','desc')->pluck('RECEIPT_NO','id')->first();
            $last_number=substr($p_date,9);
            $number = sprintf('%05d', ++$last_number);
        }
        else{
            $number = 0000;
            $number = sprintf('%05d', ++$number);
        } */
        // Gosa Code End

        $last_id = BillCollections::orderBy('id','desc')->pluck('id','id')->first();
        $number = ++$last_id;

        $consumer_no  = $request['c_code'];

        $consumer_email = $request['email_id'];
        $consumer_mobile = $request['mobile_no'];
            $result = BillItem::whereRaw("C_CODE= '$consumer_no' AND bill_date=(select max(bill_date) from bills_import where C_CODE='$consumer_no')")->orderBy('id','desc')->first();
            $bill = new BillCollections;
            $rcpno=substr($consumer_no,0,3).date('dmy').$number;
            $bill->C_CODE=$result['C_CODE'];
            $bill->BILL_NO=$result['BILL_NO'];
            $bill->PAID_AMT=$result['BILL_AMT'];
            $bill->BILLED_UNITS=$result['BILLED_UNITS'];
            $bill->BILL_DUE_DATE=$result['BILL_DUE_DATE'];
            $bill->BILL_DUE_AMT=$result['BILL_AMT'];
            $bill->SUB_DIVISION=$result['SUB_DIVISION'];
            $bill->SUB_DIVISION_DESC=$result['SUB_DIVISION'];
            $bill->RECEIPT_NO=$rcpno;
            $bill->STATUS="PENDING";
            $bill->PAID_DATE=date('Y-m-d');
            $bill->BILL_DATE=date('Y-m-d',strtotime($result['BILL_DATE']));
            $bill->DISTRICT=$result['DISTRICT'];  
            $bill->save();

        // }
        $amount = (float)$result['BILL_AMT'];
        $data="PEDGOM|".$result['C_CODE']."|NA|".$amount."|NA|NA|NA|INR|NA|R|pedgom|NA|NA|F|".$rcpno."|".$consumer_email."|".$consumer_mobile."|NA|NA|NA|NA|NA";
        $checksum_key="6bgrhMRvVFp8";
        $checksum = hash_hmac('sha256',$data,$checksum_key, false); 
        $checksum = strtoupper($checksum);
        $data=$data."|".$checksum;
        return view('payment.data',compact('data'));

        
    }

    public function history_list(Request $request)
    {
        

        $date = BillCollections:: orderBy( 'BILL_DATE' )->first();
        for ( $i = date( 'Y', strtotime( $date->BILL_DATE ) ); $i <= date( 'Y' ); $i ++ ) {
            $year[ $i ] = $i;
        }
        $yearAll     = [ '' => 'All Year' ] + $year; 

        $months = [
            '1' => 'January',
            '2' => 'February',
            '3' => 'March',
            '4' => 'April',
            '5' => 'May',
            '6' => 'June',
            '7' => 'July',
            '8' => 'August',
            '9' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December'];

        $monthAll = ['' => 'All Month'] + $months;
        if(isset($request['c_code'])){
            $c_code= (!empty($request['c_code']))?"AND c_code LIKE '%".$request['c_code']."%'":'';
            $year= (!empty($request['year']))?"AND YEAR(`BILL_DATE`) = '".$request['year']."'":'';
            $month= (!empty($request['month']))?"AND MONTH(`BILL_DATE`) = '".$request['month']."'":'';
            $history_list = BillCollections::whereRaw("id>0 $c_code $year $month"  )->orderBy( "id", "asc" )->paginate( 15 );
       }
       else{
        $history_list = BillCollections::where('id', '=', '0')->paginate(15);
       }

        return view('payment.history_list',compact('history_list','yearAll','monthAll'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($request)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getbill(Request $request)
    {
        $rules = [
                'consumer_no' => 'required'    
            ];
            $this->validate($request, $rules);
            if(!$request['g-recaptcha-response'])
            {
                $invalidCaptcha = "Please Verify the Captcha";
                return view('payment.index',compact('invalidCaptcha'));
            }
            
            $consumer_no = $request['consumer_no'];
            $remote_db=config('oracle');
            $result = BillItem::whereRaw("C_CODE= '$consumer_no' AND bill_date=(select max(bill_date) from bills_import where C_CODE='$consumer_no')")->orderBy('id','desc')->first();
            if(!$result)
            {
                   
                return redirect('https://www.powermizoram.gov.in');
            }
            else
            {
               
                return view('payment.contactmodal',compact('consumer_no'));
            }
             
    }
    public function query(Request $request)
    {
        
             
            $consumer_no = $request->consumer_no;
            if($request->email_id)
            {
                if(!filter_var(trim($request->email_id), FILTER_VALIDATE_EMAIL)) 
                {
                    $emailValidation='Incorrect email id';
                    return view('payment.contactmodal',compact('consumer_no','emailValidation'));

                }
            }
            else
            {
                $emailValidation='Email id field is required';
                return view('payment.contactmodal',compact('consumer_no','emailValidation'));

            }
            if($request->mobile_no)
            {
                if(strlen(trim($request->mobile_no))!=10) 
                {
                    $mobileValidation='Mobile no field must be 10 digits';
                    return view('payment.contactmodal',compact('consumer_no','mobileValidation'));

                }
                if (!is_numeric(trim($request->mobile_no))) 
                {
                    $mobileValidation='Mobile no field must be numeric';
                    return view('payment.contactmodal',compact('consumer_no','mobileValidation'));

                }
            }
            else
            {
                $mobileValidation='Mobile No field is required';
                return view('payment.contactmodal',compact('consumer_no','mobileValidation'));

            }
            $email_id = $request->email_id;
            $mobile_no = $request->mobile_no;
            // $remote_db=config('oracle');
            // $conn = oci_connect($remote_db['oracle']['username'], $remote_db['oracle']['password'],"//".$remote_db['oracle']['host']."/".$remote_db['oracle']['database']);
            // $count_customer = oci_parse($conn, 'select count(c_code) as countResult from customer where c_code= :code');
            // oci_bind_by_name($count_customer, ":code", $consumer_no);
            // oci_execute($count_customer);
            // $count_result=oci_fetch_array($count_customer,OCI_ASSOC);
            // if($count_result['COUNTRESULT']>0)
            // {
            //     $row = oci_parse($conn, 'select c_code,c_name,c_father_name,bill_no,bill_date,bill_from_date,bill_to_date,bill_due_date,billed_units,bill_amt from customer inner join bill using(c_code)
            //                 where c_code= :code and bill_date=(select max(bill_date) from bill where c_code=:code)');
            //     oci_bind_by_name($row, ":code", $consumer_no);
            //     oci_execute($row);
            //     $result=oci_fetch_array($row,OCI_ASSOC);
            //     return view('payment.query',compact('result','email_id','mobile_no'));
            // }
            // else
            // {

                $result = BillItem::whereRaw("C_CODE= '$consumer_no' AND bill_date=(select max(bill_date) from bills_import where C_CODE='$consumer_no')")->orderBy('id','desc')->first();
                return view('payment.query',compact('result','email_id','mobile_no'));
            // }
        

    }

    public function billpayment(Request $request)
    {
        $rules = [
                'c_code' => 'required'
             ];

        $this->validate($request, $rules);

        $input  = $request->all();
        $c_code = $request['c_code'];
        $remote_db=config('oracle');
        $conn = oci_connect($remote_db['oracle']['username'], $remote_db['oracle']['password'],"//".$remote_db['oracle']['host']."/".$remote_db['oracle']['database']);
        
        $row = oci_parse($conn, 'select c_code,c_name,c_father_name,bill_no,bill_date,bill_from_date,bill_to_date,bill_due_date,billed_units,bill_amt from customer inner join bill using(c_code)
                        where c_code= :code and bill_date=(select max(bill_date) from bill where c_code=:code)');
        oci_bind_by_name($row, ":code", $c_code);
        oci_execute($row);
        $result=oci_fetch_array($row,OCI_ASSOC);
        return view('payment.query',compact('result'));
    }

    public function billpayment_import(Request $request)
    {
        $rules = [
                'C_CODE' => 'required'
             ];

        $this->validate($request, $rules);

        $input  = $request->all();
        $C_CODE = $request['C_CODE'];
      
        $result = BillItem::whereRaw("C_CODE= '$C_CODE' AND bill_date=(select max(bill_date) from bills_import where C_CODE='$C_CODE')")->orderBy('id','desc')->first();
        return view('payment.query_import',compact('result'));
    }

    public function store_import(Request $request)
    {
        // Gosa Code Start
        /* $d_date= BillCollections::orderBy('id','desc')->pluck('PAID_DATE','id')->first();
        $c_date=date('Y-m-d');
        if($d_date == $c_date)
        {
            $p_date= BillCollections::orderBy('id','desc')->pluck('RECEIPT_NO','id')->first();
            $last_number=substr($p_date,9);
            $number = sprintf('%05d', ++$last_number);
        }
        else{
            $number = 0000;
            $number = sprintf('%05d', ++$number);
        } */
        //Gosa Code End

        $last_id = BillCollections::orderBy('id','desc')->pluck('id','id')->first();
        $number = $last_id + 1;

        $c_code  = $request['c_code']; 
        $result =BillItem::whereRaw("C_CODE ='$c_code'")->orderBy('id','desc')->first();
        $request = new BillCollections;
        $rcpno=substr($result['C_CODE'],0,3).date('dmy').$number;
        dd($rcpno);
        $request->C_CODE=$result['C_CODE'];
        $request->BILL_NO=$result['BILL_NO'];
        $request->PAID_AMT=$result['BILL_AMT'];
        $request->RECEIPT_NO=$rcpno;
        $request->PAID_DATE=date('Y-m-d');
        $request->BILL_DATE=date('Y-m-d',strtotime($result['BILL_DATE']));
        $request->save();
        $ddate=date('d-M-y');

        return redirect('billpayment/receipt/'.'?c_code='.$request['C_CODE']);
    }

    public function receipt(Request $request)
    {
        $data=explode("|", $request['msg']);
        $wsd=new webserver_details;
        $wsd->data=$request['msg'];
        $wsd->save();
        $msg_checksum=$data[sizeof($data)-1];
        array_pop($data);
        // dd($data);
        $data_checksum = hash_hmac('sha256',implode("|",$data),'6bgrhMRvVFp8', false); 
        $data_checksum = strtoupper($msg_checksum);
        if($msg_checksum==$data_checksum)
        {
             if($data[14]=='0300')
             {
                $status="SUCCESS";
                $offset=16;
                BillCollections::where('receipt_no',$data[$offset])
                                 ->update([

                                    'status'            =>  'SUCCESS',
                                    'txnreferenceno'    =>  $data[2],
                                    'bankreferenceno'   =>  $data[3],
                                    'bankid'            =>  $data[5],
                                    'nkmerchantid'       =>  $data[6],
                                    'txndate'           =>  date('Y-m-d',strtotime($data[13])),
                                    'txntype'           =>  $data[7],
                                    'email'             =>  $data[17],
                                    'mobile_no'         =>  $data[18],
                                    'txnamt'            =>  $data[4],

                                    ]);
                $xml = new \SimpleXMLElement('<xml/>');
                $xml->addChild('return','Y');
                Header('Content-type: text/xml');
                return $xml->asXML();                
            }
            else 
            {
                $status="FAILURE";
                $offset=16;
                BillCollections::where('receipt_no',$data[$offset])
                             ->update([

                                    'status'            =>  $status,
                                    'txnreferenceno'    =>  $data[2],
                                    'bankreferenceno'   =>  $data[3],
                                    'bankid'            =>  $data[5],
                                    'nkmerchantid'      =>  $data[6],
                                    'txndate'           =>  date('Y-m-d',strtotime($data[13])),
                                    'txntype'           =>  $data[7],
                                    'email'             =>  $data[17],
                                    'mobile_no'         =>  $data[18],
                                    'txnamt'            =>  $data[4],

                                    ]);
                $xml = new \SimpleXMLElement('<xml/>');
                $xml->addChild('return','N');
                Header('Content-type: text/xml');
                return $xml->asXML();
            }
        }
        else
        {
            $status="FAILURE";
             $offset=16;
            BillCollections::where('receipt_no',$data[$offset])
                             ->update([

                                    'status'            =>  $status,
                                    'txnreferenceno'    =>  $data[2],
                                    'bankreferenceno'   =>  $data[3],
                                    'bankid'            =>  $data[5],
                                    'nkmerchantid'       =>  $data[6],
                                    'txndate'           =>  date('Y-m-d',strtotime($data[13])),
                                    'txntype'           =>  $data[7],
                                    'email'             =>  $data[17],
                                    'mobile_no'         =>  $data[18],
                                    'txnamt'            =>  $data[4],

                                    ]);
            $xml = new \SimpleXMLElement('<xml/>');
            $xml->addChild('return','N');
            Header('Content-type: text/xml');
            return $xml->asXML();
        }       
    }

    public function queryPage()
    {
        $data = "PEDGOM|ARC101E034|HCIT5176224157|488495-863581|2.00|CIT|438587|03|INR|DIRECT|NA|NA|NA|09-03-2017|0300|NA|C101E0340816|gosaralte@gmail.com|8974428633|NA|NA|NA|NA|NA|Success";
        $checksum = hash_hmac('sha256',$data,'6bgrhMRvVFp8', false);
        $checksum = strtoupper($checksum);
        $data = $data."|".$checksum;
        dd($data);
        return view("payment.paynow", compact('data'));
    }
}
