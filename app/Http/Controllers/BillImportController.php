<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BillItem;
use App\Division;
use App\BillCollections;
use Excel;
use DB;
use Auth;

class BillImportController extends Controller
{
    
   
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        abort(403,'UN-AUTHORISED ACTION');
        return view('billimport.importbills');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function update(Request $request, $id)
    {
       $consumer = BillItem::find($id);
      
       BillItem::where('id','=',$id)
                    ->update(['bill_amt' => $request->amount]);
        $success='Bill amount for ' . $consumer['C_CODE'].' has been updated';
        \Session::flash('success',$success);

        return redirect('/correction');
    }

    public function billcorrection(Request $request)
    {
        if(Auth::user()->email == 'hlira@mail.com' || Auth::user()->email == 'admin@mail.com')
        {

         
            $bill_date = BillItem::count();

            if($bill_date > 0){
                $date = BillItem:: orderBy( 'BILL_DATE' )->first();
                for ( $i = date( 'Y', strtotime( $date->BILL_DATE ) ); $i <= date( 'Y' ); $i ++ ) {
                    $year[ $i ] = $i;
                }
                $yearAll     = [ '' => 'All Year' ] + $year; 
            }
            else{
                $yearAll = ['' => 'All Year'];
            }

            $months = [
                '1' => 'January',
                '2' => 'February',
                '3' => 'March',
                '4' => 'April',
                '5' => 'May',
                '6' => 'June',
                '7' => 'July',
                '8' => 'August',
                '9' => 'September',
                '10' => 'October',
                '11' => 'November',
                '12' => 'December'];

            $monthAll = ['' => 'All Month'] + $months;

            // $consumer_no= (!empty($request['consumer_no']))?"AND consumer_no LIKE '%".$request['consumer_no']."%'":'';
            // $year= (!empty($request['year']))?"AND YEAR(`bill_date`) = '".$request['year']."'":'';
            // $month= (!empty($request['month']))?"AND MONTH(`bill_date`) = '".$request['month']."'":'';
            $history_list = BillItem::where(function($query) use ($request){
                                $query->where('C_CODE','LIKE','%'.$request['C_CODE'].'%');
                                $query->whereYear('BILL_DATE','LIKE','%'.$request['year'].'%');
                                $query->whereMonth('BILL_DATE','LIKE','%'.$request['month'].'%');
                                
                            })->orderBy( "id", "asc" )->paginate( 15 );
            return View('billimport.billcorrection',compact('history_list','yearAll','monthAll'));
       } 
       else
       {
            abort(403);
       }
       

    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function success($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function import_server(Request $request)
    {
        // shell_exec(\Artisan::call('import:bills'));
        if(!$request->bill_date)
        {
             $bill_date = "Bill Date field is required";
                    return view('billimport.importbills',compact('bill_date'));
        }
        else
        {
            if(BillItem::where('BILL_DATE','=',date('Y-m-d',strtotime($request->bill_date)))->count())
            {
                $bill_date = "Bill already imported for this month";
                    return view('billimport.importbills',compact('bill_date'));
            }
            
            $bill_date= date('d-M-y',strtotime($request->bill_date));
            $date_format='DD-MON-YY';
            $remote_db=config('oracle');
            $conn = oci_connect($remote_db['oracle']['username'], $remote_db['oracle']['password'],"//".$remote_db['oracle']['host']."/".$remote_db['oracle']['database']);
            $count_customer = oci_parse($conn, 'select count(c_code) as countResult from bill where bill_date= to_date(:bill_date, :date_format)');
            oci_bind_by_name($count_customer, ":bill_date", $bill_date);
            oci_bind_by_name($count_customer, ":date_format", $date_format);
            oci_execute($count_customer);
            $count_result=oci_fetch_array($count_customer,OCI_ASSOC);
            $count = $count_result["COUNTRESULT"];

            // dd($count);
            // if($count==0);
            // {

                
            //     $bill_date = "Nothing to import for this month";
            //         return view('billimport.importbills',compact('bill_date'));
            // }
            // \Artisan::call('import:bills');
            shell_exec('php ' . base_path('artisan') . ' import:bills '. $bill_date . ' > /dev/null 2>&1 &');
            shell_exec('php ' . base_path('artisan') . ' import:printbills > /dev/null 2>&1 &');
            return view('billimport.importprogress',compact('count','bill_date'));
      
        }
    }
    public function progressCheck($bill_date,$count)
    {
        $meta = BillItem::where('BILL_DATE','=',date('Y-m-d',strtotime($bill_date)))->count();
        $response = array(
                    'count' => $meta
                     );
        if ($meta==$count) {
            $success='Bills for '.date('d-M-y',strtotime($bill_date)).' has been imported successfully';
            \Session::flash('success',$success);
        }
        return $response;  
    }
    public function importExport()
    {
        if(auth::user()->id == 1){
            return view('billimport.importExport');
        } else {
            abort(403, 'Unauthorized action.');
        }
       
    }
    public function download()
    {
        //$billDate= BillCollections::groupBy('bill_date')->pluck('bill_date','bill_date');
        $billAll= [''=>'All Month',
                    '1' => 'January',
                    '2' =>'February',
                    '3' => 'March',
                    '4' => 'April',
                    '5' => 'May',
                    '6' => 'June',
                    '7' => 'July',
                    '8' => 'August',
                    '9' => 'September',
                    '10' => 'October',
                    '11' => 'Novermber',
                    '12' => 'December'
                    ];

        $check = BillCollections:: orderBy( 'PAID_DATE' )->count();
        if($check > 0){
                $date = BillCollections::orderBy( 'PAID_DATE' )->first();
                for ( $i = date( 'Y', strtotime( $date->PAID_DATE ) ); $i <= date( 'Y' ); $i ++ ) {
                    $year[ $i ] = $i;
                }
            }
        else{
            $year = [];
        }
        $yearAll     = [ '' => 'All Year' ] + $year;


        return view('billimport.download',compact('billAll','yearAll'));
    }

    public function downloadExcel(Request $request, $type)
        {
            $data = BillCollections::where( function ( $q ) use ( $request ) {
            
                 if ( $request->has( "payYear" ) && $request->payYear != null ) {
                    $q->where( DB::raw( "YEAR(`PAID_DATE`)" ), "=", $request->payYear );
                }

                if ( $request->has( "payMonth" ) && $request->payMonth != null ) {
                    $q->where( DB::raw( "MONTH(`PAID_DATE`)" ), "=", $request->payMonth );
                    $q->where( DB::raw( "STATUS" ), "=", "success" );
                }
            } )
                           ->get()
                           ->toArray();

            return Excel::create('bill_collection', function($excel) use ($data) {
                $excel->sheet('mySheet', function($sheet) use ($data)
                {
                    $sheet->fromArray($data);
                });
            })->download($type);
        }
        
    public function importExcel(Request $request)
        {
            //if($request->hasFile('import_file'))   

            if($request->file('import_file') !=null)  
            //2nd aug 2021 thanpuia
            {
                $chunk=0;
                $path = $request->file('import_file')->getRealPath();
                $data = Excel::load($path, function($reader) {})->get();
                if(!empty($data) && $data->count())
                {
                    foreach ($data->toArray() as $key => $value) 
                    {
                            if(!empty($value))
                            {
                                $chunk++;
                                $insert[] = [   
                                                'C_CODE' => $value['c_code'], 
                                                'BILL_NO' => $value['bill_no'],
                                                'C_NAME' => $value['c_name'], 
                                                'BILL_AMT' => $value['bill_amt'],
                                                'BILL_DATE' => date('Y-m-d', strtotime($value['bill_date'])), 
                                                'SUB_DIVISION' => $value['sub_division'], 
                                                'BILLED_UNITS' => $value['billed_units'], 
                                                'BILL_TO_DATE' => date('Y-m-d', strtotime($value['bill_to_date'])),
                                                'BILL_FROM_DATE' => date('Y-m-d', strtotime($value['bill_from_date'])), 
                                                'BILL_DUE_DATE' => date('Y-m-d', strtotime($value['bill_due_date'])),
                                                'DISTRICT' => $value['district'],
                                            ];
                            }
                            if($chunk==1000)
                            {
                                if(!empty($insert)){
                                    BillItem::insert($insert);
                                    $insert=array();
                                    $chunk=0;
                                }
                            }
                    }
                    if($chunk!=0)
                    {
                        \App\BillItem::insert($insert); 
                        return back()->with('success','Successful-II Inserted');
                    }
                }
                return back()->with('success','Semi-Successful-I Data Empty');

                    
            }
            return back()->with('error','Please Check your file Something is wrong.');
        }
}
