<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentControl extends Model
{
   public $table = "payment_controls";
}
