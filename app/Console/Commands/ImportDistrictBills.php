<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportDistrictBills extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:districtbills';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Bill for All District';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $date_format='DD-MON-YY';
        $remote_db=config('oracle');
        $conn = oci_connect($remote_db['oracle']['username'], $remote_db['oracle']['password'],"//".$remote_db['oracle']['host']."/".$remote_db['oracle']['database']);
        $row = oci_parse($conn, 'select c_name,cons_no,bill_no,sub_division,billed_units,amount_due_within_due_date,bill_date,start_bill,end_bill,due_date,district from billprint_sub');
        //oci_bind_by_name($row, ":bill_date", $bill_date);
        //oci_bind_by_name($row, ":date_format", $date_format);
        //oci_bind_by_name($row, ":con", $con);
        
        oci_execute($row);
        $new_insert_array=array();
        $chunk=0;
        while ($results=oci_fetch_array($row,OCI_ASSOC)) 
        {
            $chunk++;
            $new_insert_array[]=array(
                'C_NAME'    => $results['C_NAME'],
                'C_CODE'    => $results['CONS_NO'],
                'BILL_NO'   => $results['BILL_NO'],
                'BILL_AMT'  => $results['AMOUNT_DUE_WITHIN_DUE_DATE'],
                'SUB_DIVISION'      => $results['SUB_DIVISION'],
                'C_FATHER_NAME'   => '',
                'BILLED_UNITS'    => $results['BILLED_UNITS'],
                'BILL_TO_DATE'    => date('Y-m-d',strtotime($results['END_BILL'])),
                'BILL_FROM_DATE'  => date('Y-m-d',strtotime($results['START_BILL'])),
                'BILL_DUE_DATE' =>  date('Y-m-d',strtotime($results['DUE_DATE'])),
                'BILL_DATE'     =>  date('Y-m-d',strtotime($results['BILL_DATE'])),
                'DISTRICT'      => $results['DISTRICT']    
            );
            if ($chunk==1000) {
                \App\BillItem::insert($new_insert_array);
                $new_insert_array=array();
                $chunk=0;
            }

        }
        if($chunk!=0)
        {
            \App\BillItem::insert($new_insert_array); 

        }
    }
}
