<?php
// NON RAPDRP
namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportBills extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:bills {bill_date}'; //02-JAN-19 //RADRP

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Bills from P&E database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            $bill_date = $this->argument('bill_date');
            $date_format='DD-MON-YY';
            $remote_db=config('oracle');
            $conn = oci_connect($remote_db['oracle']['username'], $remote_db['oracle']['password'],"//".$remote_db['oracle']['host']."/".$remote_db['oracle']['database']);
            $row = oci_parse($conn, 'select c_code ,c_name, billed_units,bill_no, bill_date,bill_amt,bill_due_date,bill_from_date,bill_to_date from bill left join customer using(c_code) where bill_date=to_date(:bill_date, :date_format)');
            oci_bind_by_name($row, ":bill_date", $bill_date);
            oci_bind_by_name($row, ":date_format", $date_format);
            oci_execute($row);
            $new_insert_array=array();
            $chunk=0;
            while ($results=oci_fetch_array($row,OCI_ASSOC)) 
            {
                $chunk++;
                $new_insert_array[]=array(
                    'C_NAME'    => $results['C_NAME'],
                    'C_CODE'    => $results['C_CODE'],
                    'BILL_NO'   => $results['BILL_NO'],
                    'BILL_AMT'  => $results['BILL_AMT'],
                    'SUB_DIVISION'  => '',
                    'C_FATHER_NAME'   => '',
                    'BILLED_UNITS'    => $results['BILLED_UNITS'],
                    'BILL_TO_DATE'    => date('Y-m-d',strtotime($results['BILL_TO_DATE'])),
                    'BILL_FROM_DATE'  => date('Y-m-d',strtotime($results['BILL_FROM_DATE'])),
                    'BILL_DUE_DATE' =>  date('Y-m-d',strtotime($results['BILL_DUE_DATE'])),
                    'BILL_DATE'     =>  date('Y-m-d',strtotime($results['BILL_DATE']))


                );
                if ($chunk==1000) {
                    \App\BillItem::insert($new_insert_array);
                    $new_insert_array=array();
                    $chunk=0;
                }

            }
            if($chunk!=0)
            {
                \App\BillItem::insert($new_insert_array); 

            }
    }
}
