<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BillItem;
use App\BillCollectionsBBPS;
use App\BillCollections;
use Illuminate\Support\Facades\DB;

class PrepareBills extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prepare:bills {bill_date}'; //02-JAN-19

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prepare Bill for Payment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      if ($this->confirm('Do you wish to continue?')) 
        {
            $bill_date = date('Y-m-d',strtotime($this->argument('bill_date')));
            ini_set('memory_limit', '-1');
            $billImportMonthWiseAll = BillItem::where('BILL_DATE',$bill_date)->get();
            $table = (new BillCollections)->getTable();
            //DB::select("Truncate table $table");
            BillCollections::truncate();
            $last_id = BillCollections::max('id');
            foreach($billImportMonthWiseAll as $billImport)
            {
                $number = ++$last_id;
                $consumer_no  = $billImport->C_CODE;
                $bill = new BillCollections;
                $rcpno=substr($consumer_no,0,3).date('dmy').$number;
                $bill->C_CODE   = $billImport->C_CODE ;
                $bill->BILL_NO  = $billImport->BILL_NO;
                $bill->BILLED_UNITS = $billImport->BILLED_UNITS;
                $bill->BILL_DUE_DATE = $billImport->BILL_DUE_DATE;
                $bill->BILL_DUE_AMT =  $billImport->BILL_AMT;
                $bill->SUB_DIVISION = $billImport->SUB_DIVISION;
                $bill->SUB_DIVISION_DESC = $billImport->SUB_DIVISION;
                $bill->RECEIPT_NO   = $rcpno;
                $bill->STATUS="PENDING";
                $bill->BILL_DATE=date('Y-m-d',strtotime($bill_date));
                $bill->save();

            }
            $this->info('Database Prepare Successfully');
        }


    }
}
